#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

bool arrayEqual(int m, int n, int a[m][n], int b[m][n]){
    int i, j;
    for (i = 0; i < m; i++){
        for (j = 0; j < n; j++){
            if (a[i][j] != b[i][j]){
                printf("False\n");
                return false;
            }
        }
    }
    printf("True\n");
    return true;
}

bool arrayEqual2(int **a, int **b, int m, int n){
    int i, j;
    for (i = 0; i < m; i++){
        for (j = 0; j < n; j++){
            if (a[i][j] != b[i][j]){
                printf("False\n");
                return false;
            }
        }
    }
    printf("True\n");
    return true;
}

int main(){
    //FIXED
    int m = 2;
    int n = 3;
    int a[2][3] = {{1, 2, 3}, {3, 2, 1}};
    int b[2][3] = {{1, 2, 3}, {3, 2, 0}};
    arrayEqual(m, n, a, b);

    //DYNAMIC
    int i, j;
    int **a2;
    int **b2;
    a2 = (int**)malloc(m * sizeof(int*));
    b2 = (int**)malloc(m * sizeof(int*));
    for (i = 0; i < m; i++){
        a2[i] = (int*)malloc(n * sizeof(int));
        b2[i] = (int*)malloc(n * sizeof(int));
    }
    for (i = 0; i < m; i++){
        for (j = 0; j < n; j++){
            a2[i][j] = 1;
            b2[i][j] = 0;
        }
    }
    arrayEqual2(a2, b2, m, n);
    for (i = 0; i < m; i++){
        free(a2[i]);
        free(b2[i]);
    }
    free(a2);
    free(b2);

    //END
    return 0;
}