#include <stdlib.h>
#include <stdio.h>
#include "reversi.h"

void main(){
    BoardType board;
    boardAlloc(&board);
    initBoard(&board);
    playBoard(&board);
    cleanBoard(&board);
}