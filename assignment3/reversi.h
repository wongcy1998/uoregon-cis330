#ifndef REVERSI_H_
#define REVERSI_H_

typedef struct {
    char **boardArray;
    int size;
    enum colours{Empty, Black, White}colour;
} BoardType;

void boardAlloc(BoardType *board);

void initBoard(BoardType *board);

void playBoard(BoardType *board);

void printBoard(BoardType *board);

void cleanBoard(BoardType *board);

#endif /* REVERSI_H_ */