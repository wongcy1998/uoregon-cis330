#include <stdlib.h>
#include <stdio.h>
#include "reversi.h"

void boardAlloc(BoardType *board){
    int i, size;    
    char *p, buffer[BUFSIZ];
    //User prompt input for array size.
    InputLoop:
    printf("Please enter board size larger than 2: ");
    if(fgets(buffer, sizeof(buffer), stdin) != NULL){
        size = strtol(buffer, &p, 10);
        if (size > 2){
            if (buffer[0] != '\n' && (*p == '\n' || *p == '\0')){
                printf("Successfully entered %d.\n", size);
            }
        } else {
            goto InputLoop;
        }
    }
    board->size = size;
    //Memory allocating for the board array.
    board->boardArray = (char**)malloc(size * sizeof(char*));
    for (i = 0; i < size; i++){
        board->boardArray[i] = (char*)malloc(size * sizeof(char));
    }
}

void initBoard(BoardType *board){
    int i, j;
    int size = board->size;
    //Input the array with genuic values.
    for (i = 0; i < size; i++){
        for (j = 0; j < size; j++){
            board->boardArray[i][j] = '.';
        }
    }
    //Calculate the 4 original positions.
    int mid1 = (size / 2) -1;
    int mid2 = mid1 + 1;
    //Plot the 4 original positions.
    board->boardArray[mid1][mid1] = 'O';
    board->boardArray[mid1][mid2] = '#';
    board->boardArray[mid2][mid1] = '#';
    board->boardArray[mid2][mid2] = 'O';
}

void playBoard(BoardType *board){
    int ifEnd = 0;
    int MAX = board->size;
    int MAX_total = MAX * MAX;
    int i, COL, ROW, current_player, previous_player, holder;
    int counter[3] = {0, 2, 2};
    char index[3] = {' ', '#', 'O'};
    char *p, *q, bufferCOL[BUFSIZ], bufferROW[BUFSIZ];
    printBoard(board);
    printf("\nThe game will start with Player 1.\n\n");
    //Enum to start game with Black first.
    current_player = Black;
    previous_player = White;
    while (ifEnd != 1){
        //Player turn.
        printf("Current Score:\n");
        printf("Black: %d , White: %d\n", counter[Black], counter[White]);
        printf("\nIt is Player %d's turn.\n", current_player);
        
        //User prompt.
        ROWCOLLoop:
        printf("\nYou will be prompted to enter timer columns and rows seperately.\n");
        printf("Please enter target column: ");
        if (fgets(bufferCOL, sizeof(bufferCOL), stdin) != NULL){
            COL = strtol(bufferCOL, &p, 10);
            if (bufferCOL[0] != '\n' && (*p == '\n' || *p == '\0')){
                printf("Please enter target row: ");
                if(fgets(bufferROW, sizeof(bufferROW), stdin) != NULL){
                    ROW = strtol(bufferROW, &q, 10);
                    if ((COL >= 0) && (COL < MAX) && (ROW >= 0) && (ROW < MAX)){
                        if (bufferROW[0] != '\n' && (*q == '\n' || *q == '\0')){
                            if ((board->boardArray[ROW][COL] != '#') && (board->boardArray[ROW][COL] != 'O')){
                                printf("You have succesfully entered row %d and column %d.\n", ROW, COL);
                            } else {
                                printf("###ERROR Position already has a value.\n");
                                goto ROWCOLLoop;
                            }
                        } else {
                            printf("###ERROR Please correct your input.\n");
                            goto ROWCOLLoop;
                        }
                    } else {
                        printf("###ERROR Please correct your input.\n");
                        goto ROWCOLLoop;
                    }
                } else {
                    printf("###ERROR Please correct your input.\n");
                    goto ROWCOLLoop;
                }
            } else {
                printf("###ERROR Please correct your input.\n");
                goto ROWCOLLoop;
            }
        }

        //Passed customs, substitute in.
        board->boardArray[ROW][COL] = index[current_player];
        counter[current_player] += 1;

        //Game logic.
        //North
        if (ROW > 1){
            if (board->boardArray[ROW-2][COL] == index[current_player]){
                if (board->boardArray[ROW-1][COL] == index[previous_player]){
                    board->boardArray[ROW-1][COL] = index[current_player];
                    counter[current_player] += 1;
                    counter[previous_player] -= 1;
                }
            }
        }
        
        //South
        if (ROW < MAX - 2){
            if (board->boardArray[ROW+2][COL] == index[current_player]){
                if (board->boardArray[ROW+1][COL] == index[previous_player]){
                    board->boardArray[ROW+1][COL] = index[current_player];
                    counter[current_player] += 1;
                    counter[previous_player] -= 1;
                }
            }
        }
        
        //East
        if (COL < MAX - 2){
            if (board->boardArray[ROW][COL+2] == index[current_player]){
                if (board->boardArray[ROW][COL+1] == index[previous_player]){
                    board->boardArray[ROW][COL+1] = index[current_player];
                    counter[current_player] += 1;
                    counter[previous_player] -= 1;
                }
            }
        }
        
        //West
        if (COL > 1){
            if (board->boardArray[ROW][COL-2] == index[current_player]){
                if (board->boardArray[ROW][COL-1] == index[previous_player]){
                    board->boardArray[ROW][COL-1] = index[current_player];
                    counter[current_player] += 1;
                    counter[previous_player] -= 1;
                }
            }
        }
        
        //North-East
        if (ROW > 1){
            if (COL < MAX - 2){
                if (board->boardArray[ROW-2][COL+2] == index[current_player]){
                    if (board->boardArray[ROW-1][COL+1] == index[previous_player]){
                        board->boardArray[ROW-1][COL+1] = index[current_player];
                        counter[current_player] += 1;
                        counter[previous_player] -= 1;
                    }
                }
            }
        }
        
        //North-West
        if (ROW > 1){
            if (COL > 1){
                if (board->boardArray[ROW-2][COL-2] == index[current_player]){
                    if (board->boardArray[ROW-1][COL-1] == index[previous_player]){
                        board->boardArray[ROW-1][COL-1] = index[current_player];
                        counter[current_player] += 1;
                        counter[previous_player] -= 1;
                    }
                }
            }
        }
        
        //South-East
        if (ROW < MAX-2){
            if (COL < MAX-2){
                if (board->boardArray[ROW+2][COL+2] == index[current_player]){
                    if (board->boardArray[ROW+1][COL+1] == index[previous_player]){
                        board->boardArray[ROW+1][COL+1] = index[current_player];
                        counter[current_player] += 1;
                        counter[previous_player] -= 1;
                    }
                }
            }
        }
        
        //South-West
        if (ROW < MAX-2){
            if (COL > 1){
                if (board->boardArray[ROW+2][COL-2] == index[current_player]){
                    if (board->boardArray[ROW+1][COL-1] == index[previous_player]){
                        board->boardArray[ROW+1][COL-1] = index[current_player];
                        counter[current_player] += 1;
                        counter[previous_player] -= 1;
                    }
                }
            }
        }

        printBoard(board);

        //If board is full.
        int current_full = counter[Black] + counter[White];
        if (current_full == (MAX_total)){
            ifEnd = 1;
            if (counter[Black] > counter[White]){
                printf("Game End! Winner is Player %d!\n", Black);
            } else if (counter[White] > counter[Black]){
                printf("Game End! Winner is Player %d!\n", White);
            }
            printf("With a Score of Black: %d , White: %d !\n", counter[Black], counter[White]);
        }
        
        //Ready for next round, swap player counter.
        if (current_player == Black){
            current_player = White;
            previous_player = Black;
        } else if (current_player == White){
            current_player = Black;
            previous_player = White;
        }
    }
}

void printBoard(BoardType *board){
    int i,j,k;
    int size = board->size;
    printf("\n  ");
    for (k = 0; k < size; k++){
        printf(" %d ", k);
    }
    printf("\n");
    for (i = 0; i < size; i++){
        printf("%d ", i);
        for (j = 0; j < size; j++){
            printf(" %c ", board->boardArray[i][j]);
        }
        printf("\n");
    }
    printf("\n");
}

void cleanBoard(BoardType *board){
    int i;
    int size = board->size;
    for (i = 0; i < size; i++){
        free(board->boardArray[i]);
    }
    free(board->boardArray);
}