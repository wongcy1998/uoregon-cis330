#include <iostream>

int main(int argc, char **argv){
    int size = 5, i, j;
    int **num = nullptr;
    num = new int * [size];
    for (int i = 0; i < size; i++){
        num[i] = new int[size];
    }

    for (i = 0; i < size; i++){
        for (j = 0; j < size; j++){
            num[i][j] = i * size + j;
        }
    }

    for (i = 0; i < size; i++){
        for (j = 0; j < size; j++){
            std::cout << num[i][j] << " ";
        }
        std::cout << "\n";
    }

    for (i = 0; i < size; i++)
		delete [] num[i];
	delete [] num;
}