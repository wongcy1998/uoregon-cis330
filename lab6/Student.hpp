#include <string>;

using namespace std;

class Student{
    public:
    Student();
    Student(Student& Existing);
    Student(Student&& Existing);
    ~Student();    
    bool SetAge(int);
    
    private:
    string firstName, lastName;
    int age;
}