#include <string>
#include <iostream>
#include "Student.hpp"

using namespace std;

class Student{
    Student(){
        this->firstName = "";
        this->lastName = "";
        this->age = 0;
    }
    Student(Student& existing){
        this->firstName = existing.lastName;
        this->lastName = existing.lastName;
        this->age = existing.age;
    }
    Student(Student&& existing){
        this->firstName = existing.lastName;
        this->lastName = existing.lastName;
        this->age = existing.age;
    }
    Student(string X){
        string a, b;
        cout << "Please Enter the Givenname of Student: ";
        cin >> a;
        cout << "Please Enter the Surname of Student: ";
        cin >> b;
        this->firstName = a;
        this->lastName = b;
    }
    ~Student(){    }
    
    private:
    string firstName, lastName;
    int age;
}