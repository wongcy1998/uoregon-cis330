#include <iostream>
#include <string>
#include <limits>
#include "reversi.hpp"

using namespace std;

namespace reversi{

    MainBoard::MainBoard(){
        int i, j;
        this->size = 0;

        //User prompt input for array size.
        InputLoop:
        cout << "Please enter board size larger than 2: ";
        if (!(cin >> this->size)){
            cin.clear();
            cin.ignore(numeric_limits<streamsize>::max(), '\n');
            goto InputLoop;
        }
        if (this->size < 3){
            cin.clear();
            goto InputLoop;
        }
        
        //Memory allocating for the board array.
        this->boardArray = new char * [this->size];
        for (i = 0; i < this->size; i++){
            this->boardArray[i] = new char[this->size];
            for (j = 0; j < size; j++){
                this->boardArray[i][j] = '.';
            }
        }

        //Calculate the 4 original positions.
        int mid1 = (this->size / 2) -1;
        int mid2 = mid1 + 1;

        //Plot the 4 original positions.
        this->boardArray[mid1][mid1] = 'O';
        this->boardArray[mid1][mid2] = '#';
        this->boardArray[mid2][mid1] = '#';
        this->boardArray[mid2][mid2] = 'O';
    }

    void MainBoard::playBoard(){
        int ifEnd = 0;
        int MAX = this->size;
        int MAX_total = MAX * MAX;
        int i, j, current_player, previous_player, FLAG, twoFlag, holder_i, minv, rowc, colc;
        int counter[3] = {0, 2, 2};
        char index[3] = {' ', '#', 'O'};
        printBoard();
        cout << "\nThe game will start with Player 1.\n\n";
        //Enum to start game with Black first.
        current_player = Black;
        previous_player = White;
        while (ifEnd != 1){
            //Player turn.
            cout << "Current Score:\n";
            cout << "Black: " << counter[Black] << " , White: " << counter[White] << "\n";
            cout << "\nIt is Player " << current_player << "'s turn.\n";
            
            //User prompt.
            InputLoop:
            cin.clear();
            cout << "\nYou will be prompted to enter timer columns and rows seperately.\n";
            cout << "Please enter target column: ";
            if(!(cin >> this->COL)){
                cin.clear();
                cin.ignore(numeric_limits<streamsize>::max(), '\n');
                goto InputLoop;
            }
            if ((this->COL < 0) || (this->ROW >= this->size)){
                cin.clear();
                this->COL = 0;
                goto InputLoop;
            }
            cout << "Please enter target row: ";
            if(!(cin >> this->ROW)){
                cin.clear();
                cin.ignore(numeric_limits<streamsize>::max(), '\n');
                goto InputLoop;
            }
            if ((this->ROW < 0) || (this->ROW >= this->size)){
                cin.clear();
                this->ROW = 0;
                goto InputLoop;
            }
            cin.clear();
            //Check if occupied
            if (this->boardArray[this->ROW][this->COL] != '.'){
                cout << "Error, space is occupied";
                this->ROW = 0;
                this->COL = 0;
                goto InputLoop;
            }

            twoFlag = 0;
            //Game logic.
            //North
            FLAG = 0;
            if (this->ROW > 1){
                for (i = (this->ROW - 1); i >= 0; i--){
                    if((this->boardArray[i][this->COL] == index[current_player]) && (FLAG == 0)){
                        FLAG = 1;
                        holder_i = i+1;
                        break;
                    } else if (this->boardArray[i][this->COL] == '.'){
                        break;
                    }
                }
                if (FLAG == 1){
                    twoFlag = 1;
                    for (i = holder_i; i < this->ROW; i++){
                        this->boardArray[i][this->COL] = index[current_player];
                        counter[current_player] += 1;
                        counter[previous_player] -= 1;
                    }
                }
            }
            
            //South
            FLAG = 0;
            if (this->ROW < MAX - 2){
                for (i = (this->ROW + 1); i < MAX; i++){
                    if((this->boardArray[i][this->COL] == index[current_player]) && (FLAG == 0)){
                        FLAG = 1;
                        holder_i = i-1;
                        break;
                    } else if (this->boardArray[i][this->COL] == '.'){
                        break;
                    }
                }
                if (FLAG == 1){
                    twoFlag = 1;
                    for (i = holder_i; i > this->ROW; i--){
                        this->boardArray[i][this->COL] = index[current_player];
                        counter[current_player] += 1;
                        counter[previous_player] -= 1;
                    }
                }
            }
            
            //East
            FLAG = 0;
            if (this->COL > 1){
                for (i = (this->COL - 1); i >= 0; i--){
                    if((this->boardArray[this->ROW][i] == index[current_player]) && (FLAG == 0)){
                        FLAG = 1;
                        holder_i = i+1;
                        break;
                    } else if (this->boardArray[this->ROW][i] == '.'){
                        break;
                    }
                }
                if (FLAG == 1){
                    twoFlag = 1;
                    for (i = holder_i; i < this->ROW; i++){
                        this->boardArray[this->ROW][i] = index[current_player];
                        counter[current_player] += 1;
                        counter[previous_player] -= 1;
                    }
                }
            }
            
            //West
            FLAG = 0;
            if (this->COL < MAX - 2){
                for (i = (this->COL + 1); i < MAX; i++){
                    if((this->boardArray[this->ROW][i] == index[current_player]) && (FLAG == 0)){
                        FLAG = 1;
                        holder_i = i-1;
                        break;
                    } else if (this->boardArray[this->ROW][i] == '.'){
                        break;
                    }
                }
                if (FLAG == 1){
                    twoFlag = 1;
                    for (i = holder_i; i > this->COL; i--){
                        this->boardArray[this->ROW][i] = index[current_player];
                        counter[current_player] += 1;
                        counter[previous_player] -= 1;
                    }
                }
            }

            //North-East
            FLAG = 0;
            if ((this->ROW > 1) && (this->COL > 1)){
                rowc = this->ROW;
                colc = this->COL;
                if (rowc < colc){
                    minv = rowc;
                } else {
                    minv = colc;
                }
                for (i = 1; i <= minv; i++){
                    if((this->boardArray[this->ROW-i][this->COL-i] == index[current_player]) && (FLAG == 0)){
                        FLAG = 1;
                        holder_i = i;
                        break;
                    } else if (this->boardArray[this->ROW-i][this->COL-i] == '.'){
                        break;
                    }
                }
                if ((FLAG == 1)&&(holder_i != 1)){
                    twoFlag = 1;
                    for (i = 1; i <= holder_i; i++){
                        this->boardArray[this->ROW-i][this->COL-i] = index[current_player];
                        counter[current_player] += 1;
                        counter[previous_player] -= 1;
                    }
                }
            }
            
            //Passed customs, substitute in.
            this->boardArray[this->ROW][this->COL] = index[current_player];
            counter[current_player] += 1;

            //North-West
            FLAG = 0;
            if ((this->ROW > 1) && (this->COL < MAX - 2)){
                rowc = this->ROW;
                colc = MAX - this->COL - 1;
                if (rowc < colc){
                    minv = rowc;
                } else {
                    minv = colc;
                }
                for (i = 1; i <= minv; i++){
                    if((this->boardArray[this->ROW-i][this->COL+i] == index[current_player]) && (FLAG == 0)){
                        FLAG = 1;
                        holder_i = i;
                        break;
                    } else if (this->boardArray[this->ROW-i][this->COL+i] == '.'){
                        break;
                    }
                }
                if ((FLAG == 1)&&(holder_i != 1)){
                    twoFlag = 1;
                    for (i = 1; i <= holder_i; i++){
                        this->boardArray[this->ROW-i][this->COL+i] = index[current_player];
                        counter[current_player] += 1;
                        counter[previous_player] -= 1;
                    }
                }
            }
            
            //South-East
            FLAG = 0;
            if ((this->ROW < MAX - 2) && (this->COL > 1)){
                rowc = MAX - this->ROW - 1;
                colc = MAX - this->COL - 1;
                if (rowc < colc){
                    minv = rowc;
                } else {
                    minv = colc;
                }
                for (i = 1; i <= minv; i++){
                    if((this->boardArray[this->ROW+i][this->COL+i] == index[current_player]) && (FLAG == 0)){
                        FLAG = 1;
                        holder_i = i-1;
                        break;
                    } else if (this->boardArray[this->ROW+i][this->COL+i] == '.'){
                        break;
                    }
                }
                if ((FLAG == 1)&&(holder_i != 1)){
                    twoFlag = 1;
                    for (i = 1; i <= holder_i; i++){
                        this->boardArray[this->ROW+i][this->COL+i] = index[current_player];
                        counter[current_player] += 1;
                        counter[previous_player] -= 1;
                    }
                }
            }

            //South-West
            FLAG = 0;
            if ((this->ROW < MAX - 2) && (this->COL < MAX - 2)){
                rowc = MAX - this->ROW - 1;
                colc = this->COL;
                if (rowc < colc){
                    minv = rowc;
                } else {
                    minv = colc;
                }
                minv = MAX - this->ROW - 1;
                for (i = 1; i <= minv; i++){
                    if((this->boardArray[this->ROW+i][this->COL-i] == index[current_player]) && (FLAG == 0)){
                        FLAG = 1;
                        holder_i = i;
                        break;
                    } else if (this->boardArray[this->ROW=i][this->COL-i] == '.'){
                        break;
                    }
                }
                if ((FLAG == 1)&&(holder_i != 1)){
                    twoFlag = 1;
                    for (i = 1; i <= holder_i; i++){
                        this->boardArray[this->ROW-i][this->COL+i] = index[current_player];
                        counter[current_player] += 1;
                        counter[previous_player] -= 1;
                    }
                }
            }

            if (twoFlag == 0){
                this->boardArray[this->ROW][this->COL] = '.';
                goto InputLoop;
            }
            
            
            printBoard();

            //If board is full.
            int current_full = counter[Black] + counter[White];
            if (current_full == (MAX_total)){
                ifEnd = 1;
                if (counter[Black] > counter[White]){
                    cout << "Game End! Winner is Player " << Black << " !\n";
                } else if (counter[White] > counter[Black]){
                    cout << "Game End! Winner is Player " << White << " !\n";
                }
                cout << "With a Score of Black: " << counter[Black] << " , White: " << counter[White] << " !\n";
            }
            
            //Ready for next round, swap player counter.
            if (current_player == Black){
                current_player = White;
                previous_player = Black;
            } else if (current_player == White){
                current_player = Black;
                previous_player = White;
            }

        }
    }

    void MainBoard::printBoard(){
        int i,j,k;
        cout << "\n ";
        for (k = 0; k < this->size; k++){
            cout << " " << k;
        }
        cout << "\n";
        for (i = 0; i < this->size; i++){
            cout << i << " ";
            for (j = 0; j < size; j++){
                cout << this->boardArray[i][j] << " ";
            }
            cout << "\n";
        }
        cout << "\n";
    }

    MainBoard::~MainBoard(){
        int i;
        int size = size;
        for (i = 0; i < size; i++){
            delete[] this->boardArray[i];
        }
        delete[] this->boardArray;
    }
}