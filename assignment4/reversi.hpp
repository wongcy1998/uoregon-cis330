namespace reversi{
    class MainBoard{
    public:
        MainBoard();
        ~MainBoard();
        void playBoard();
        void printBoard();
        char **boardArray;
        int size, COL, ROW;
        enum colours{Empty, Black, White};
    };
};