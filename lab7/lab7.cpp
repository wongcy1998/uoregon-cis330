#include <iostream>
#include <random>
#include <vector>
#include <algorithm>

int printv(const std::vector<int> v){
    for (std::vector<int>::const_iterator vit= v.begin();
        vit != v.end();
        ++vit){
        std::cout << *vit << std::endl;
    }
}

int main(){
    std::vector<int> v, vCopy;
    std::default_random_engine generator;
    std::uniform_int_distribution<int> distribution(0, 15);
    for (int i = 0; i < 15; i++){
        int holder = distribution(generator);
        v.push_back(holder);
    }
    printv(v);
    std::sort(v.begin(), v.end());
    std::cout << std::endl;
    printv(v);

    std::copy(v.begin(), v.end(), std::back_inserter(vCopy));

    std::random_shuffle(vCopy.begin(), vCopy.end());
    std::cout << std::endl;
    printv(vCopy);
}