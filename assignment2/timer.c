#include <stdlib.h>
#include <stdio.h>
#include <time.h>
#include <unistd.h>

#include "timer.h"
#include "clock.c"

//Initilisation and Print.
void initTimer(ClockType *clock, int minutes, int seconds){
    //Memory allocate for final array.
    initClock(clock);
    
    //Parse minutes and seconds into time_t element.
    int i;
    int int_holder = (minutes * 60) + seconds;
    time_t t_holder = int_holder;
    time_t timer_time = mktime(gmtime(&t_holder));

    //Print the clock for specific time.
    for (i = 0; i < int_holder; i++){
        printClock(timer_time, clock);
        sleep(1);
        timer_time -= 1;
    }
    printClock(timer_time, clock);
}

void runTimer(){
}

//Deep clean, 250 dollars please.
void cleanTimer(ClockType  *clock){
    cleanClock(clock);
}