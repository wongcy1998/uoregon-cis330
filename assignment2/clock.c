#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <time.h>

#include "clock.h"

//Initialisation
void initClock(ClockType *clock){
    int i;
    //Memory allocation for final 2D array.
    clock->clockArray = (char**)malloc(8 * sizeof(char*));
    for (i = 0; i < 8; i++){
        clock->clockArray[i] = (char*)malloc(56 * sizeof(char));
    }
}

//Print
void printClock(const time_t cur_time, const ClockType *clock){
    //3D array of ASCII for respective numbers and characters.
    char ascii[11][8][7] = {
                            {
                                {"  ###  "},
                                {" #   # "},
                                {" #   # "},
                                {" #   # "},
                                {" #   # "},
                                {" #   # "},
                                {" #   # "},
                                {"  ###  "},
                            },{
                                {"   #   "},
                                {"  ##   "},
                                {"   #   "},
                                {"   #   "},
                                {"   #   "},
                                {"   #   "},
                                {"   #   "},
                                {"  ###  "}
                            },{
                                {"  ###  "},
                                {" #   # "},
                                {"     # "},
                                {"     # "},
                                {"    #  "},
                                {"   #   "},
                                {"  #    "},
                                {" ##### "}
                            },{
                                {"  ###  "},
                                {" #   # "},
                                {"     # "},
                                {"    #  "},
                                {"     # "},
                                {"     # "},
                                {" #   # "},
                                {"  ###  "}
                            },{
                                {"    #  "},
                                {"   ##  "},
                                {"  # #  "},
                                {" ##### "},
                                {"    #  "},
                                {"    #  "},
                                {"    #  "},
                                {"    #  "}
                            },{
                                {" ##### "},
                                {" #     "},
                                {" #     "},
                                {" ####  "},
                                {"     # "},
                                {"     # "},
                                {" #   # "},
                                {"  ###  "}
                            },{
                                {"  ###  "},
                                {" #   # "},
                                {" #     "},
                                {" ####  "},
                                {" #   # "},
                                {" #   # "},
                                {" #   # "},
                                {"  ###  "}
                            },{
                                {" ##### "},
                                {"     # "},
                                {"    #  "},
                                {"    #  "},
                                {"   #   "},
                                {"   #   "},
                                {"  #    "},
                                {"  #    "}
                            },{
                                {"  ###  "},
                                {" #   # "},
                                {" #   # "},
                                {"  ###  "},
                                {" #   # "},
                                {" #   # "},
                                {" #   # "},
                                {"  ###  "}
                            },{
                                {"  ###  "},
                                {" #   # "},
                                {" #   # "},
                                {"  #### "},
                                {"     # "},
                                {"     # "},
                                {"    #  "},
                                {"   #   "}
                            },{
                                {"       "},
                                {"       "},
                                {"   @   "},
                                {"       "},
                                {"       "},
                                {"   @   "},
                                {"       "},
                                {"       "}
                            }
    };
    int i, j, k, l;

    /* 
     * 3-way for-loop, i for position of time, j, k for re-allocation.
     * Allocation consists from ASCII 3D array to clock final 2D array.
     */
    for (i = 0; i < 8; i++){
        int pointer = i*7;
        if ((i != 2) & (i != 5)){
            l = ctime(&cur_time)[i+11] - '0';
        } else {
            l = 10;
        }
        for (j = 0; j < 8; j++){
            for (k = 0; k < 7; k++){
                clock->clockArray[j][k+pointer] = ascii[l][j][k];
            }
        }
    }

    //Printing final 2D array stored in clock.
    printf("\n");
    for (i = 0; i < 8; i++){
        printf("%.56s\n", clock->clockArray[i]);
    }
    printf("\n");
}

//Clean
void cleanClock(ClockType *clock){
    int i;
    for (i = 0; i < 8; i++){
        free(clock->clockArray[i]);
    }
    free(clock->clockArray);
}