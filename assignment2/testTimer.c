#include <stdlib.h>
#include <stdio.h>
#include <time.h>

#include "timer.h"

void main(){
    int min, sec;
    //User inputs, allows under 60 seconds, unlimited minutes.
    printf("You will be prompted to enter timer minutes and seconds seperately.\nPlease enter timer minutes runtime: ");
    scanf("%d", &min);
    printf("Please enter timer seconds runtime: ");
    scanf("%d", &sec);
    while ((sec >= 60) | (sec < 0)){
        printf("ERROR: You will be prompted to enter timer minutes and seconds seperately.\nPlease enter timer minutes runtime: ");
        scanf("%d", &min);
        printf("Please enter timer seconds runtime: ");
        scanf("%d", &sec);
    }

    ClockType *clock;
    initTimer(clock, min, sec);
    cleanTimer(clock);
}