#include <ncurses.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <time.h>
#include <unistd.h>

//Code for clock.
typedef struct {
    char **clockArray;
} ClockType;

void initClock(ClockType *clock){
    int i;
    clock->clockArray = (char**)malloc(8 * sizeof(char*));
    for (i = 0; i < 8; i++){
        clock->clockArray[i] = (char*)malloc(56 * sizeof(char));
    }
}

void printClock(const time_t cur_time, const ClockType *clock){
    char ascii[11][8][7] = {
                            {
                                {"  ###  "},
                                {" #   # "},
                                {" #   # "},
                                {" #   # "},
                                {" #   # "},
                                {" #   # "},
                                {" #   # "},
                                {"  ###  "},
                            },{
                                {"   #   "},
                                {"  ##   "},
                                {"   #   "},
                                {"   #   "},
                                {"   #   "},
                                {"   #   "},
                                {"   #   "},
                                {"  ###  "}
                            },{
                                {"  ###  "},
                                {" #   # "},
                                {"     # "},
                                {"     # "},
                                {"    #  "},
                                {"   #   "},
                                {"  #    "},
                                {" ##### "}
                            },{
                                {"  ###  "},
                                {" #   # "},
                                {"     # "},
                                {"    #  "},
                                {"     # "},
                                {"     # "},
                                {" #   # "},
                                {"  ###  "}
                            },{
                                {"    #  "},
                                {"   ##  "},
                                {"  # #  "},
                                {" ##### "},
                                {"    #  "},
                                {"    #  "},
                                {"    #  "},
                                {"    #  "}
                            },{
                                {" ##### "},
                                {" #     "},
                                {" #     "},
                                {" ####  "},
                                {"     # "},
                                {"     # "},
                                {" #   # "},
                                {"  ###  "}
                            },{
                                {"  ###  "},
                                {" #   # "},
                                {" #     "},
                                {" ####  "},
                                {" #   # "},
                                {" #   # "},
                                {" #   # "},
                                {"  ###  "}
                            },{
                                {" ##### "},
                                {"     # "},
                                {"    #  "},
                                {"    #  "},
                                {"   #   "},
                                {"   #   "},
                                {"  #    "},
                                {"  #    "}
                            },{
                                {"  ###  "},
                                {" #   # "},
                                {" #   # "},
                                {"  ###  "},
                                {" #   # "},
                                {" #   # "},
                                {" #   # "},
                                {"  ###  "}
                            },{
                                {"  ###  "},
                                {" #   # "},
                                {" #   # "},
                                {"  #### "},
                                {"     # "},
                                {"     # "},
                                {"    #  "},
                                {"   #   "}
                            },{
                                {"       "},
                                {"       "},
                                {"   @   "},
                                {"       "},
                                {"       "},
                                {"   @   "},
                                {"       "},
                                {"       "}
                            }
    };
    int i, j, k, l;
    for (i = 0; i < 8; i++){
        int pointer = i*7;
        if ((i != 2) & (i != 5)){
            l = ctime(&cur_time)[i+11] - '0';
        } else {
            l = 10;
        }
        for (j = 0; j < 8; j++){
            for (k = 0; k < 7; k++){
                clock->clockArray[j][k+pointer] = ascii[l][j][k];
            }
        }
    }
    printw("\n");
    for (i = 0; i < 8; i++){
        printw("%.56s\n", clock->clockArray[i]);
    }
    printw("\n");
    refresh();
}

//Code for timer.
void initTimer(ClockType *clock, int minutes, int seconds){
    initClock(clock);
    
    int i;
    int int_holder = (minutes * 60) + seconds;
    time_t t_holder = int_holder;
    time_t timer_time = mktime(gmtime(&t_holder));

    for (i = 0; i < int_holder; i++){
        clear();
        printClock(timer_time, clock);
        sleep(1);
        timer_time -= 1;
    }
    clear();
    printClock(timer_time, clock);
}

void cleanTimer(ClockType *clock){
    int i;
    for (i = 0; i < 8; i++){
        free(clock->clockArray[i]);
    }
    free(clock->clockArray);
}

void main(){
    
    int row, col, min, sec;
    ClockType *clock;

    printf("You will be prompted to enter timer minutes and seconds seperately.\nPlease enter timer minutes runtime: ");
    scanf("%d", &min);
    printf("Please enter timer seconds runtime: ");
    scanf("%d", &sec);
    while ((sec >= 60) | (min >= 100) | (sec < 0) | (min < 0)){
        printf("ERROR: You will be prompted to enter timer minutes and seconds seperately.\nPlease enter timer minutes runtime: ");
        scanf("%d", &min);
        printf("Please enter timer seconds runtime: ");
        scanf("%d", &sec);
    }

    initscr();                      //Initialise Screen
    
    initTimer(clock, min, sec);     //Initialise Timer
    cleanTimer(clock);              //Clean Timer

    printw("Press any key to exit.");
    getch();
    clear();
    endwin();                       //So Long, Fare Well, It's Time To Say Good Bye!
}