#include <stdlib.h>
#include <stdio.h>
#include <time.h>

#include "clock.h"

void main(){
    time_t cur_time;
    ClockType *clock;

    initClock(clock);

    printClock(time(&cur_time), clock);

    cleanClock(clock);
}