#ifndef DATE_HPP_
#define DATE

#include "cipher.hpp"

class DateCipher : public Cipher{
    public:
    DateCipher();
    virtual std::string encrypt( std::string &text );
    virtual std::string decrypt( std::string &text );

    private:
    std::string date;
    std::string date_holder;
};

#endif /* DATE_HPP_ */