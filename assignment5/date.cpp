#include <string>
#include <iostream>
#include "cipher.hpp"
#include "date.hpp"

DateCipher::DateCipher() : Cipher(), date("12/18/46") {
    /* //Optional user input, remove comments if desired functionality.
    std::cout << "Please enter date (MM/DD/YY): ";
    std::cin >> date;
    std::cout << std::endl;
     */
    for (int i = 0; i < date.length(); i++){
        if (date[i] != '/'){
            date_holder += date[i];
        }
    }
}

std::string DateCipher::encrypt(std::string &inputText){
    std::string text = inputText, numtext = inputText;
	std::string::size_type len = text.length();

    //create string of numbers based on date
    int flag = 0;
    for (int i = 0; i < len; i++){
        if(isalpha(numtext[i])) {
            numtext[i] = date_holder[(i + flag) % 6];
        } else {
            flag -= 1;
        }
    }

    //rotate through the string
    for (int i = 0; i < len; i++){
        //shifting individually
        if (isalpha(text[i])){
            for (int j = 0; j < numtext[i] - '0'; j++){
                if (text[i] == 'z'){
                    text[i] = 'a';
                } else if (text[i] == 'Z'){
                    text[i] = 'A';
                } else {
                    text[i] += 1;
                }
            }
        }
    }
    return text;
}

std::string DateCipher::decrypt(std::string &inputText){
    std::string text = inputText, numtext = inputText;
	std::string::size_type len = text.length();

    //create string of numbers based on date
    int flag = 0;
    for (int i = 0; i < len; i++){
        if(isalpha(numtext[i])) {
            numtext[i] = date_holder[(i + flag) % 6];
        } else {
            flag -= 1;
        }
    }

    //rotate through the string
    for (int i = 0; i < len; i++){
        //shifting individually
        if (isalpha(text[i])){
            for (int j = 0; j < numtext[i] - '0'; j++){
                if (text[i] == 'a'){
                    text[i] = 'z';
                } else if (text[i] == 'A'){
                    text[i] = 'Z';
                } else {
                    text[i] -= 1;
                }
            }
        }
    }
    return text;
}