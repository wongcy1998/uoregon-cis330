#include <iostream>
#include <fstream>
#include <algorithm>

#include "ioutils.hpp"
#include "cipher.hpp"
#include "caesar.hpp"
#include "rot13cipher.hpp"

int main(int argc, const char *argv[]){

	IOUtils io;
    CaesarCipher cc1;
	Rot13Cipher rot13;

	io.openStream(argc,argv);
	std::string input, decryptedc, decryptedr;
	input = io.readFromStream();
	io.closeStream();
	std::cout << "Original text:" << std::endl << input;

    decryptedc = cc1.decrypt(input);
	decryptedr = rot13.decrypt(input);
	std::cout << "Decrypted text by Caesar: " << std::endl << decryptedc << std::endl;
	std::cout << "Decrypted text by rot13: " << std::endl << decryptedr << std::endl;
    return 0;
}