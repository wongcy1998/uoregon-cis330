#include <string>
#include <iostream>
#include "cipher.hpp"
#include "caesar.hpp"

CaesarCipher::CaesarCipher() : Cipher(), rotation(13) {
    std::cout << "Please enter desired shifting value: ";
    std::cin >> rotation;
}

std::string CaesarCipher::encrypt(std::string &inputText){
    std::string text = inputText;
	std::string::size_type len = text.length();
    //rotate through the string
    for (int i = 0; i < len; i++){
        //shifting individually
        for (int j = 0; j < rotation; j++){
            if (text[i] == 'z'){
                text[i] = ' ';
            } else if (text[i] == 'Z'){
                text[i] = 'A';
            } else if (text[i] == ' '){
                text[i] = 'a';
            } else if (isalpha(text[i])){
                text[i] += 1;
            }
        }
    }
    return text;
}

std::string CaesarCipher::decrypt(std::string &inputText){
    std::string text = inputText;
	std::string::size_type len = text.length();
    //rotate through the string
    for (int i = 0; i < len; i++){
        //shifting individually
        for (int j = 0; j < rotation; j++){
            if (text[i] == 'a'){
                text[i] = ' ';
            } else if (text[i] == 'A'){
                text[i] = 'Z';
            } else if (text[i] == ' '){
                text[i] = 'z';
            } else if (isalpha(text[i])) {
                text[i] -= 1;
            }
        }
    }
    return text;
}