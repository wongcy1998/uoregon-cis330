#include <iostream>
#include <fstream>

#include "ioutils.hpp"
#include "cipher.hpp"
#include "date.hpp"

int main(int argc, const char *argv[]){
    
	IOUtils io;
    DateCipher dc1;

	io.openStream(argc,argv);
	std::string input, encrypted, decrypted;
	input = io.readFromStream();
	io.closeStream();
	std::cout << "Original text:" << std::endl << input;
    
    encrypted = dc1.encrypt(input);
	std::cout << "Encrypted text: " << std::endl << encrypted << std::endl;
    decrypted = dc1.decrypt(encrypted);
	std::cout << "Decrypted text: " << std::endl << decrypted << std::endl;

	//matching
    if (decrypted == input) std::cout << "Decrypted text matches input!" << std::endl;
	else {
		std::cout << "Oops! Decrypted text doesn't match input!" << std::endl;
		return 1;
	}
    return 0;
}