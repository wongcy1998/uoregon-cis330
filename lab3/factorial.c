#include <stdio.h>
#include <math.h>

int factorial(int n){
    int sum = 0;
    while (n > 0){
        sum *= n;
        n -= 1;
    }
    return sum;
}